<?php
/**
 * @file
 * Drush commands for the dnode_faye module.
 */

/**
 * Implements hook_drush_command().
 */
function dnode_faye_drush_command() {
  $items['dnode-faye-publish'] = array(
    'callback' => 'drush_dnode_faye_publish',
    'description' => 'Publish a JSON message to a channel',
  );
  $items['dnode-set-channel-default-perms'] = array(
    'callback' => 'drush_dnode_faye_setChannelDefaultPermissions',
    'description' => 'Set default permissions can_publis',
  );
  $items['dnode-set-channel-backlog'] = array(
    'callback' => 'drush_dnode_faye_setChannelBacklog',
    'description' => 'Enable/set or disable backlog on a channel',
  );
  return $items;
}

/**
 * Publish a message to a channel.
 */
function drush_dnode_faye_publish($channel, $message) {
  if ($m = drupal_json_decode($message)) {
    $message = $m;
  }
  drush_print("Sending to $channel " . print_r($message, TRUE));
  dnode_faye_publish($channel, $message);
}

/**
 * Set default permissions for a channel (can subscribe, can publish)
 */
function drush_dnode_faye_setChannelDefaultPermissions($channel, $can_subscribe, $can_publish) {
  $message = array('can_subscribe' => (bool) $can_subscribe, 'can_publish' => (bool) $can_publish);
  dnode_rpc('dnode_faye', 'setChannelDefaultPermissions', array($channel, $message), FALSE);
}

/**
 * Set backlog ("channel memory")
 */
function drush_dnode_faye_setChannelBacklog($channel, $backlog = FALSE) {
  if ($backlog) {
    drush_print(t('Enabling backlog (@backlog messages) on channel @channel',
      array('@backlog' => $backlog, '@channel' => $channel))
    );
  }
  else {
    drush_print(t('Disabling backlog on channel @channel',
      array('@backlog' => $backlog, '@channel' => $channel))
    );
  }
  dnode_rpc('dnode_faye', 'setChannelBacklog', array($channel, $backlog), FALSE);
}
