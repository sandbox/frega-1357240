<?php
/**
 * @file
 * Admin page callback file for the dnode_faye module.
 */

/**
 * Returns the system settings form.
 */
function dnode_faye_settings() {
  $form = array();
  $form['dnode_faye_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL to the Faye HTTP-endpoint/mount'),
    '#default_value' => variable_get('dnode_faye_url', 'http://' . $_SERVER['HTTP_HOST'] . ':8000/faye'),
    '#description' => t('Enter the URL without a trailing slash - unless you use a proxy this should correspond to the data in the ssjs/config.json file!'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}
