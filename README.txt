==============================
INSTALLATION
==============================
NOTE: You need to install the dnode-integration module from here first:

http://drupal.org/sandbox/frega/1321342

Please follow the instructions in the INSTALL.txt.

For dnode_faye you'll need a recent version of Faye (jcoglan's lovely faye[1]
comet server).

$ npm install -g faye

For server-side persistence you will need the nodejs-redis library and a redis
server[3] installed.

$ npm install -g redis

Under debian/ubuntu do the following:

$ sudo apt-get install redis-server

In the folder ssjs/ you need to rename/copy *EITHER*

  EIHTER:
    ssjs/config.dist.json-memory
  OR
    ssjs/config.dist.json-redis

to

ssjs/config.json

Enable the module and

$ drush en -y dnode_faye
$ drush dnode-nodejs-servers


dnode_faye drush commands:

  dnode-faye-publish [CHANNEL] [MESSAGE as JSON]
    directly publish a message to a channel via drush (via dnode). no
    authentication needed.

  dnode-set-channel-default-perms [CHANNEL] [SUBSCRIBE] [PUBLISH]
    Configure channel-specific (default) permissions (can be override with
    auth-tokens)

    The second and third parameter determine whethter *unauthenticated* and/or
    *unauthorised* users can SUBSCRIBE or PUBLISH by default. Examples:

    dnode-set-channel-default-perms /public-subscribeable 1 0
    dnode-set-channel-default-perms /public-publishable 0 1
    dnode-set-channel-default-perms /public-pubsub 1 1

  dnode-set-channel-backlog [CHANNEL] [NO. MESSAGES IN BACKLOG]
    Configure channel-specific message-backlog

By default every single channel subscription (on every page-load) needs a
drupal-generated auth-token. Use "dnode-set-channel-default-perms" to change
this default behavior per channel (no wildcards) allowing unauthorised (even
unauthenticated) users to subscribe and even publish.

By default a channel has no memory or state - but you can configure a backlog
per channel (no wildcards).  Use "dnode-set-channel-backlog" to configure if and
how many messages are kept (at most).

Note: Currently this is *not* persisted, i.e. everytime you the restart
dnode_faye-server *all* configuration is gone!

Submodules:
- dnode_faye_notify
  modelled on nodejs.modules' nodejs_notify - uses growl-esque notifications to
  display messages.

  drush commands:
    dnode-faye-notify "SUBJECT" "BODY" -> broadcast to everyone
    dnode-faye-notify-user "SUBJECT" "BODY" -> broadcast to everyone

- dnode_watchdog
  another small php example - pump every watchdog entry to users w/ 'access site
  reports' - permission

- dnode_faye_example
  a small ssjs example - pumping bieber-tweets via faye to all connected browser
  clients.

- dnode_faye_channelsubscription
  persistent subscriptions - small convenience module that allows to have roles
  and users to automagically (re-)subscribed to channels on every page-reload.

  drush commands:
    dnode-faye-cs-add [TYPE: user|role] [ID user-id|role-id] [CHANNEL]
    dnode-faye-cs-remove [TYPE: user|role] [ID user-id|role-id] [CHANNEL]


- dnode_faye_chatterbox
  a small chatterbox implementation demonstrating how to allow clients to
  *publish* in real-time, i.e. w/o having to go via good-ol' drupal. adds a
  block and a permission.

  drush commands (none) but you can use dnode-faye-publish to "write" to the
  chatterbox (in one line!).
    dnode-faye-publish /dnode_faye_chatterbox \
      '{"body": "A", "name": "B", "callback": "dnode_faye_chatterbox"}'

- dnode_faye_views
  initial views-cache-plugin (extending the time-based cache), allow any cache
  update (cache_set-action) to be published.

  how to use:
  1) create or edit a view (e.g. recent comments blocks)
  2) configure the cache to use the "Time-based (broadcast via faye-pubsub)"
     cache plugin. Use a ridiculously low option (1sec) for "Rendered output".
  3) place e.g. the comments block on the front page
  4) open two browsers - every time the block is re-rendered the result should
     be "refreshed" in the other browser windows as well.

- dnode_faye_csi
  provides "client side includes", a special kind of "iframe" :)
  implements three example blocks:
  - example_public
      a block that subscribes to a "public" channel that doesn't require
      to test:
        1) place dnode_faye_csi-example_public block somewhere
        2) enable anonymous caching (NOTE: all other modules currently *don't
           place nice w/ full page caches*)
        3) run this drush command:
            drush dnode-faye-csi-publish "SOME MESSAGE"
        4) open the page in a browser as anonymous user - and you'll see the
           block content being replaced by "SOME MESSAGE"

        5) run this drush command

           dnode-faye-recurringpoll-test /dnode_faye_csi/public

           This command "pumps" messages every 1 to 10 seconds into the page.

  - example:
      same but w/ an "authorised" channel

  - live node block
      a block that can be configured to re-render any time the node is updated
      or comments are added to the node. Think "live-blogging".


- dnode_faye_recurringpoll
  example module that demonstrates how to "pipe" external datasources directly
  in to "drupal pages"

  - dnode-faye-recurringpoll-test /channel
      php-based data push, enables channel backlog (1 message).

  - dnode-faye-recurringpoll-start /dnode_faye_csi/public http://TEST/t.php 100
      every *100ms*  a GET-request is sent to http://TEST/t.php
      every time the HTTP-Response body changes this is then sent via Faye to
      the channel /dnode_faye_csi/public

  - dnode-faye-recurringpoll-stop /dnode_faye_csi/public
      stops the recurring poll


NOTE:
Currently subscriptions/authentication between drupal and faye are *not*
cached. On every page reload new subscription tokens are sent via  dnode to faye
and then "passed along" by the client/browser (Javascript) to the pubsub/comet
server for (re-)authentication and (re-)authorisation.

This is obviously not very efficient but should and could be improved on fairly
easily. but it requires some architectural decisions as to where and how the
subscriptions etc. are stored (and invalidated).

BTW - why dnode?
This module does not currently use "a lot" of dnode; e.g. subscription tokens
could also easily be sent via e.g. HTTP (cf. nodejs.module); this could even
happen in a fairly efficient way using http://drupal.org/project/httprl. But
for more complex integration using a proper RPC library might be better after
all.

[1] http://faye.jcoglan.com
[2] http://drupal.org/sandbox/frega/1321342
[3] http://redis.io
