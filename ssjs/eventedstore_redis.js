/**
 * Simple wrapper for redis.
 */
var events = require('events');
var redis  = redis = require("redis");

var debug = false;

EventedStoreRedis = function(config) {
  config = config || {};
  this.redisClient = redis.createClient(config.port || 6379, config.host || '127.0.0.1', config);
  this.prefix = config.prefix || '';
  this.debug = false;
};


// "inherit" EventEmitter
EventedStoreRedis.prototype = new events.EventEmitter;

EventedStoreRedis.prototype.isset = function(key, cb) {
  this.redisClient.get(this.prefix + key, function(err, data) {
    if (err) {
      cb(err, null);
    } else {
      var r = JSON.parse(data);
      cb(err, r );
    }
  });
}

EventedStoreRedis.prototype.set = function(key, value, cb) {
  this.emit('set', key, value);
  this.redisClient.set(this.prefix + key, JSON.stringify(value), cb);
};

EventedStoreRedis.prototype.setex = function(key, ttl, value, cb) {
  this.redisClient.setex(this.prefix + key, ttl, JSON.stringify(value), cb);
};

EventedStoreRedis.prototype.setnx = function(key, value, cb) {
  this.redisClient.setnx(this.prefix + key, JSON.stringify(value), cb);
};

EventedStoreRedis.prototype.get = function(key, cb) {
  this.redisClient.get(this.prefix + key, function(err, data) {
    if (err) {
      cb(err, null);
    } else {
      cb(null, JSON.parse(data));
    }
  });
};
EventedStoreRedis.prototype.unset = function(key, cb) {
  this.emit('unset', key);
  this.redisClient.del(this.prefix + key, cb);
};
// modelled on redis' HSET - set a field in a hash
EventedStoreRedis.prototype.hset = function(key, field, value, cb) {
  this.redisClient.hset(this.prefix + key, field, JSON.stringify(value), cb);
}
// modelled on redis' HGET - set a field in a hash
EventedStoreRedis.prototype.hget = function(key, field, cb) {
  this.redisClient.hget(this.prefix + key, field, function(err, data) {
    if (err) {
      cb(err, null);
    } else {
      cb(null, JSON.parse(data));
    }
  });
}


EventedStoreRedis.prototype.sadd = function(key, member, cb) {
  this.redisClient.sadd(this.prefix + key, member, cb);
}

EventedStoreRedis.prototype.srem = function(key, member, cb) {
  this.redisClient.srem(this.prefix + key, member, cb);
}

EventedStoreRedis.prototype.smembers = function(key, cb) {
  this.redisClient.smembers(this.prefix + key, cb);
}

EventedStoreRedis.prototype.scard = function(key, cb) {
  this.redisClient.scard(this.prefix + key, cb);
}

EventedStoreRedis.prototype.sinter = function(key, key2_wo_prefix, cb) {
  this.redisClient.sinter(this.prefix + key, key2_wo_prefix, cb);
}


EventedStoreRedis.prototype.zadd = function(key, score, member, cb) {
  this.redisClient.zadd(this.prefix + key, score, member, cb);
}

EventedStoreRedis.prototype.zrem = function(key, member, cb) {
  this.redisClient.zrem(this.prefix + key, member, cb);
}

EventedStoreRedis.prototype.zrangebyscore = function(key, from_score, to_score, cb) {
  this.redisClient.zrangebyscore(this.prefix + key, from_score, to_score, cb);
}

EventedStoreRedis.prototype.keys = function(cb) {
  this.redisClient.keys(cb);
}

exports.EventedStoreRedis = EventedStoreRedis;