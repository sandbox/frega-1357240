var debug = false;

if (DrupalDnode) {
  var fayeConfig = DrupalDnode.getModuleConfig('dnode_faye');
} else {
  var fayeConfig = require('./config.json');
}

/* basic logging extension */
var ServerLoggerExtension = {
  incoming: function(message, callback) {
    console.log( 'Incoming message', message );
    return callback(message);
  },
  outgoing: function(message, callback) {
    console.log( 'Outgoing message',  message );
    return callback(message);
  }
}

exports.FayeServerLogger = {
  faye: null,
  attach: function(faye) {
    debug && console.log('FayeServerLogger attaching to Faye');
    FayeServerLogger.faye = faye;
    faye.addExtension( ServerLoggerExtension );
    debug && console.log('FayeServerLogger finished attaching to faye ');
  }
};
