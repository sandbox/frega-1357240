var EventedStore = require( './eventedstore').EventedStore;
var util = require('util');

// in ms; 10 secs for token lifetime is plenty
var authTokenStoreLifetime = 10000;

if (DrupalDnode) {
  var fayeConfig = DrupalDnode.getModuleConfig('dnode_faye');
} else {
  var fayeConfig = require('./config.json');
}

var config = fayeConfig['fayeServerAuth'];
var debug = config.debug;

var fayeServerAuthExtension = {
  incoming: function(message, callback) {
    // all server-side message and all handshaking
    if ( message.clientId == fayeServerAuth.serverClientId || message.channel == '/meta/handshake' || message.channel === '/meta/connect' || message.channel == '/meta/disconnect') {
      return callback(message);
    }
    // auth if subscription
    if (message.channel === '/meta/subscribe') {
      // Get subscribed channel and auth token
      var subscription = message.subscription,
          msgToken     = message.ext && message.ext.authToken;
      if (!msgToken) {
        debug && console.log('SUBscription incoming without token - ', message.clientId + ': ' + message.subscription);
        fayeServerAuth.canPublicSubscribe(message.subscription, message, function(err, can_subscribe) {
          debug && console.log('SUBscription incoming without token - ', message.clientId + ': ' + message.subscription, err, can_subscribe);
          if (!err && can_subscribe === true) {
            debug && console.log('SUBscription incoming without token - PUBLIC CAN SUBSCRIBE to channel: ' + message.subscription);
            return callback(message);
          }
          if (config.channelsDefaultPermission.can_subscribe == true) {
            return callback(message);
          }
          message.error = 'No auth token given - not a channel';
          return callback(message);
        });
        return ;
      }
      debug && console.log('SUBscription incoming with ' + message.clientId + ': ' + message.subscription + ':' + msgToken);

      // check token or if no token present
      fayeServerAuth.checkToken(msgToken, message, function(err, data) {
        if (err) {
          message.error = err.toString();
        } else if (!data) {
          message.error = 'Invalid auth token - token not found.';
        } else if ( data.channel != subscription ) {
          message.error = 'Invalid auth token - token not valid for channel.';
        }
        // @todo: cache client-auth and store publish-permissions on channels (s.below)
        return callback(message);
      });
    } else {
      debug && console.log('PUBlication incoming:', message.clientId + ': ' + message.channel);
      fayeServerAuth.canClientPublish(message, function(err, message) {
        if (err) {
          if (!message) { message = {}; }
          message.error = err.toString();
        }
        // @todo: filter for R/W access by client
        return callback(message);
      });
    }
  }
};


/* server authentication und authorisation */
var fayeServerAuth = {
  faye: null,
  serverClientId: null,
  authStore: null,
  channelPermissionStore: null,
  clientChannelPermissionStore: null,
  clientUserDataStore: null,
  attach: function(faye) {
    debug && console.log('fayeServerAuth attaching to faye ');
    fayeServerAuth.faye = faye;
    // set the privileged client for circumventing serverAuth protections
    // acquire a distinct server-side client id which is privileged
    // trigger getting an ID
    faye.getClient().publish('/ping');
    fayeServerAuth.setPrivilegedClientId( faye.getClient().getClientId() );
    //
    fayeServerAuth.authStore = EventedStore.createStore(config.authStore);
    fayeServerAuth.channelPermissionStore = EventedStore.createStore(config.channelPermissionStore);
    fayeServerAuth.clientChannelPermissionStore = EventedStore.createStore(config.clientChannelPermissionStore);
    fayeServerAuth.clientUserDataStore = EventedStore.createStore(config.clientUserDataStore);

    faye.addExtension( fayeServerAuthExtension );
    debug && console.log('fayeServerAuth finished attaching to faye ');


    faye.bind('disconnect', fayeServerAuth.onDisconnect );
  },
  onDisconnect: function(clientId) {
    debug && console.log('onDisconnect', clientId);
    // cleanup
    fayeServerAuth.clientUserDataStore.unset(clientId, function() {});
    fayeServerAuth.clientChannelPermissionStore.unset(clientId, function() {});
  },
  // set's a privileged
  setPrivilegedClientId: function(id) {
    fayeServerAuth.serverClientId = id;
  },
  setAuthorisationTokens: function(tokens, cb) {
    debug && console.log('setAuthorisationTokens', tokens);
    // Store auth tokens, default to 10s. That should be plenty.
    var ttl = config.authStore.tokenTTL || 10;
    for (var token in tokens) {
      fayeServerAuth.authStore.setex(token, ttl, tokens[token], function() {});
    }
    // @todo Callback only after this has completed?
    // We should consider using async or something like that here, because
    // otherwise there is a (slight) risk of a race condition. Considering that
    // this has to go back to Drupal-land the danger isn't very high.
    cb(true);
  },
  canClientPublish: function(message, cb) {
    if (!message.clientId) {
      return cb(new Error('Publishing denied - no clientId'), message);
    }
    debug && console.log('canClientPublish', message);
    fayeServerAuth.getChannelDataByClientId(message.clientId, message.channel, function(err, data) {
      if (err) {
        debug && console.log('canClientPublish - error ', err, message);
        return cb(err, message);
      }
      // clientId specific check
      debug && console.log('canClientPublish - retrieved clientId ' + message.clientId + '\'s data for channel ' + message.channel, data );
      if (data && data.can_publish) {
        return cb(null, message);
      }
      // channel specific check
      fayeServerAuth.canPublicPublish(message.channel, message, function(err, can_publish) {
        if (!err && can_publish === true) {
          return cb(null, message);
        }
        // global fallback check
        if (config.channelsDefaultPermission.can_publish) {
          return cb(null, message);
        } else {
          return cb(new Error('Publishing denied - clientId is not allowed'));
        }
      });
    });
  },

  /**
   * Tries to pickup a token for a subscription, if there is an data associated with the token it will be put in the
   *
   * Currently each subscription has one token. Maybe we should
   * @param token
   * @param cb
   */
  checkToken: function(token, message, cb) {
    debug && console.log('checkToken - token, message', token, message);
    fayeServerAuth.authStore.get(token, function(err, data) {
      debug && console.log('fayeServerAuth.authStore.get', err, data);
      if (err||!data) {
        debug && console.log('checkToken -> authStore.get - error:', err);
        return cb(err, data);
      }
      debug && console.log('fayeServerAuth.authStore.get(' + token + ')', data);
      // store clientId <-> user Session data
      if (data.user) {
        // let's store this only once per client Authentication/Authorisation
        fayeServerAuth.clientUserDataStore.setnx(message.clientId, data.user, function(err, r) {
          debug && console.log('checkToken -> setnx clientChannelPermissionStore', message.clientId, data.channel, data.user);
        });
      }
      // store channel X clientId specific-data info[clientId][channel] = DATA
      // subscription / publication info
      fayeServerAuth.clientChannelPermissionStore.hset(message.clientId, data.channel, data.data, function(err, r) {
        debug && console.log('checkToken -> hset clientChannelPermissionStore - data', util.inspect(data) );
        debug && console.log('checkToken -> hset clientChannelPermissionStore', message.clientId, data.channel, data.data);
      });
      cb(err, data);
    });
  },
  // {{ CHANNEL DEFAULT PERMISSIONS
  getChannelDefaultPermissions: function(channel, cb) {
    fayeServerAuth.channelPermissionStore.get( channel, cb);
  },
  setChannelDefaultPermissions: function(channel, data, cb) {
    debug && console.log('setChannelDefaultPermissions- for channel', channel, data);
    // @todo: *no* wild-cards currently
    // @todo: make the way this is determined more configurable i.e.
    // - deny, allow, callback,
    // can_subscribe: nobody, token, callback, httpCallback, dnode, public
    // can_publish: nobody, token, callback, httpCallback, dnode, public
    if (data===false) {
      fayeServerAuth.channelPermissionStore.unset( channel );
    } else {
      fayeServerAuth.channelPermissionStore.set( channel, data, cb);
    }
  },

  /**
   * we're already including the message so that we prepare for
   * more flexible checks (callbacks).
   */
  canPublicSubscribe: function(channel, message, cb) {
    // debug && console.log('canPublicSubscribe - for channel', channel, message);
    fayeServerAuth.getChannelDefaultPermissions( channel, function(err, data) {
      debug && console.log('canPublicSubscribe - for channel - ', channel, message, err, data);
      if (err) {
        return cb(err, data);
      } else {
        debug && console.log('canPublicSubscribe - for channel', channel, message, data );
        data = data || {};
        return cb(null, data.can_subscribe === true);
      }
    });
  },
  canPublicPublish: function(channel, message, cb) {
    debug && console.log('canPublicPublish - for channel', channel, message);
    fayeServerAuth.getChannelDefaultPermissions( channel, function(err, data) {
      if (err) {
        return cb(err, data);
      } else {
        debug && console.log('canPublicPublish - for channel', channel, message, data );
        return cb(null, data.can_publish === true);
      }
    });
  },
  // {{ User Data by Client Id
  getUserDataByClientId: function(clientId, cb) {
    fayeServerAuth.clientUserDataStore.get(clientId, cb);
  },
  /// {{ Channel Permsisions Data  By Client Id
  getChannelDataByClientId: function(clientId, channel, cb) {
    fayeServerAuth.clientChannelPermissionStore.hget(clientId, channel, cb);
  }
};

exports.fayeServerAuth = fayeServerAuth;
