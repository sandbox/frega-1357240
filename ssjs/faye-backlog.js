/**
 * Faye backlog implementation
 *
 * Keeps/persists an optional backlog per channel
 * Sends this data on successful subscription to the subscriber.
 */


// @todo:
//  - weight the extensions so that we can ensure that this extension is *always* last to make sure we store what ends up going over the wire.

var EventedStore = require( './eventedstore' ).EventedStore;
if (DrupalDnode) {
  var fayeConfig = DrupalDnode.getModuleConfig('dnode_faye');
} else {
  var fayeConfig = require('./config.json');
}

// @todo: how to do proper dependency injection ...
var config = fayeConfig['fayeBacklog'];
var debug = config.debug;

var fayeBacklog = {
  faye: null,
  backlogChannelStore: null,
  backlogDataStore: null,
  setChannelBacklog: function(channel, backlogSize, cb) {
    if (backlogSize) {
      fayeBacklog.backlogChannelStore.set(channel, backlogSize, cb);
    } else {
      fayeBacklog.backlogChannelStore.unset(channel, cb);
    }
  },
  isBacklogChannel: function(channel, cb) {
    fayeBacklog.backlogChannelStore.get(channel, cb);
  },
  getChannelBacklogData: function(channel, cb) {
    return fayeBacklog.backlogDataStore.get(channel, cb);
  },
  setChannelBacklogData: function(channel, data, cb) {
    return fayeBacklog.backlogDataStore.set(channel, data, cb);
  },
  appendToChannelBacklogData: function(channel, data, backlogSize, cb) {
    // @todo: if we used a store with "capped-lists" we would delegate this ...
    fayeBacklog.getChannelBacklogData(channel, function(err, bl) {
      if (err) {
        bl = [];
      } else {
        bl = bl || [];
      }
      // cap' it.
      if (bl.length >= backlogSize) {
        bl.shift();
      }
      bl.push( data );
      debug && console.log('appendToChannelBacklogData', channel, data, backlogSize, bl.length);
      fayeBacklog.setChannelBacklogData(channel, bl, cb);
    });
  },
  onPublish: function(clientId, channel, data) {
    // onPublish
    debug && console.log('onPublish', clientId, channel, data);
    fayeBacklog.isBacklogChannel(channel, function(err, backlogSize) {
      if (err||!backlogSize) {
        debug && console.log('isBacklogChannel(channel:' + channel +') FALSE backlogSize==0 (or null)');
      } else {
        debug && console.log('isBacklogChannel', channel, err, backlogSize);
        fayeBacklog.appendToChannelBacklogData(channel, data, backlogSize, function() {});
      }
    });
  },
  /* @todo: we should clean up old data for channels that don't exist anymore? */
  garbageCollection: function() {}
};

var fayeBacklogExtension = {
  outgoing: function(message, callback) {
    // if we have a successful subscription
    if (message.channel == '/meta/subscribe' && message.successful) {
      // let's see if we have and provide backlog on subscription
      fayeBacklog.isBacklogChannel(message.subscription, function(err, backlogSize) {
        // if this ain't, don't mix w/ it.
        if (err||!backlogSize) {
          return callback(message);
        }
         // if it is, let's get it and add to message.ext
        fayeBacklog.getChannelBacklogData(message.subscription, function(err, data) {
          if (err||!data) {
            return callback(message);
          }
          message.ext = message.ext || {};
          message.ext.backlog = data;
          debug && console.log('fayeBacklogExtension /meta/subscribe added ext ' + message.subscription, backlogSize, message);
          callback(message);
        });
      });
    }  else {
      callback(message);
    }
  }
}


fayeBacklog.attach = function(faye) {
  debug && console.log('fayeBacklog attaching to faye ');
  fayeBacklog.faye = faye;
  fayeBacklog.backlogChannelStore = EventedStore.createStore(config.backlogChannelStore);
  fayeBacklog.backlogDataStore = EventedStore.createStore(config.backlogDataStore);
  faye.addExtension( fayeBacklogExtension );
  faye.bind('publish', fayeBacklog.onPublish );
  debug && console.log('fayeBacklog finished attaching to faye ');
}

exports.fayeBacklog = fayeBacklog;