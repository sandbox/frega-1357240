var http = require('http');
var faye = require('faye');
var dnode = require('faye');
var util = require('util');

if (DrupalDnode) {
  var config = DrupalDnode.getModuleConfig('dnode_faye');
} else {
  // load configuration
  var config = require('./config.json');
}
// debug
var debug = config.debug;

var bayeux = new faye.NodeAdapter({mount: config.mount, timeout: config.timeout});
bayeux.listen(config.faye_http_port);

// add server auth
var fayeServerAuth = require('./faye-serverauth.js').fayeServerAuth;
fayeServerAuth.attach(bayeux);

// add a backlog
var fayeBacklog = require('./faye-backlog.js').fayeBacklog;
fayeBacklog.attach(bayeux);

// if enabled add "social" presence: dnode_socialgraph+dnode_presence+dnode_faye
if (config.fayeSocialPresence && config.fayeSocialPresence.enabled) {
  var FayeSocialPresence = require('./faye-socialpresence.js').FayeSocialPresence;
  FayeSocialPresence.attach(bayeux);
}

// publish
exports.publish = function(channel, message, cb) {
  debug && console.log('dnode_faye - server::publish', channel, message);
  bayeux.getClient().publish(channel, message);
  cb(null);
};

// authorisation
exports.setAuthorisationTokens = function(tokens, cb) {
  return fayeServerAuth.setAuthorisationTokens(tokens, cb);
}

exports.setChannelDefaultPermissions = function( channel, data, cb ) {
  fayeServerAuth.setChannelDefaultPermissions(channel, data, cb );
};

exports.getUserDataByClientId = function( clientId, cb ) {
  fayeServerAuth.getUserDataByClientId(clientId, cb );
}

// backlog
exports.setChannelBacklog = function( channel, backlogSize, cb ) {
  fayeBacklog.setChannelBacklog( channel, backlogSize, cb );
};

// extensions
exports.addExtension = function( extension, cb ) {
  bayeux.addExtension(extension);
  cb(null);
};

exports.removeExtension = function( extension, cb ) {
  bayeux.removeExtension(extension);
  cb(null);
};

// expose configuration
exports.config = {
  serverId: 'dnode_faye',
  port: 5051
};