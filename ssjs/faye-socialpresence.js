var async = require('async');
var dnode = require('dnode');
var _ = require('underscore');

if (DrupalDnode) {
  var fayeConfig = DrupalDnode.getModuleConfig('dnode_faye');
} else {
  var fayeConfig = require('./config.json');
}

var config = fayeConfig.fayeSocialPresence;
var debug = config.debug || false;

function publishMessageToFollowers(socialgraphIds, a, subject, body, cb) {
  async.map(socialgraphIds, function(socialgraph_id, async_cb) {
    debug && console.log('Get followers in socialgraph: ' + socialgraph_id +  ' for user #' + a);
    // @todo: refactor this to be within one connection?
    DrupalDnode.connectByServerId('dnode_socialgraph', function(remote, conn) {
      // @todo: we'd need a "getFollowersIntersect" in order to only notify
      // followers that are "online", i.e. in the dnode_presence "online" status
      // set.
      remote.getFollowers(socialgraph_id, a, function(err, data) {
        // Close connection so that we don't have anything lingering.
        conn.end();
        async_cb(err, data);
      });
    });
  }, function(err, results) {
    if (err) {
      return cb(err);
    }
    var results = _.uniq(_.flatten(results) );
    debug && console.log('Flattened and uniqued set of users to notify ', results);
    var m = {
      'subject': subject,
      'body': body,
      'callback': 'dnode_faye_notify'
    };
    // notify users
    var client = FayeSocialPresence.faye.getClient();
    for (var i=0, len = results.length; i<len; i++) {
      client.publish('/user/' + results[i], m);
      debug && console.log('Sent presence update to /user/' + results[i], m);
    }
    cb(null, results);
  });
}


/* basic social presence extension */
var SocialPresenceExtension = {
  incoming: function(message, callback) {
    if (message.channel && message.channel.indexOf('/dnode_presence') !== -1 ) {
      debug && console.log( 'Incoming presenceChange on ' + message.channel, message.data, config, config.socialgraphIds );
      var uid = message.data.id;
      var username = message.data.payload.name;
      if (!uid || !username) {
        // ignore this, if we can't build a proper message.
        return callback(message);
      }
      var body;
      var subject = 'Presence Change';
      if (message.data.status == 'online') {
        body = username + ' came online!';
      } else {
        body = username + ' went offline!';
      }
      debug && console.log('Broadcast', subject, body);
      publishMessageToFollowers(config.socialgraphIds, uid, subject, body, function(err, data) {
        callback(message);
      });
    } else {
      return callback(message);
    }
  }
}

var FayeSocialPresence = {
  faye: null,
  attach: function(faye) {
    debug && console.log('FayeSocialPresence attaching to Faye');
    FayeSocialPresence.faye = faye;
    faye.addExtension( SocialPresenceExtension );
    debug && console.log('FayeSocialPresence finished attaching to faye ');
  }
};

exports.FayeSocialPresence = FayeSocialPresence;