/*
 * Very basic (in-memory) asynchronous key-value store extending an EventEmitter
 *
 */
var events = require('events');

var EventedStoreMemory = function() {
  this.cache = {};
  this.ex ={};
  this.debug = false;
};

EventedStoreMemory.prototype = new events.EventEmitter;
EventedStoreMemory.prototype.isset = function(key, cb) {
  return cb(null, typeof this.cache[key] !== 'undefined' );
}

EventedStoreMemory.prototype.set = function(key, value, cb) {
  this.emit('set', key, value);
  this.cache[key] = value;
  return cb && cb();
};


// @todo: GC!
EventedStoreMemory.prototype.setex = function(key, ttl, value, cb) {
  var self = this;
  this.set(key, value, function(err, r) {
    self.ex[ key ] = +( new Date() ) + ( ttl * 1000 );
    this.debug && console.log( 'setex', self.ex[key] );
    self.emit('setex', key, value);
    return cb && cb();
  });
};

// modelled on redis' SETNX
EventedStoreMemory.prototype.setnx = function(key, value, cb) {
  var self = this;
  this.isset(key, function(err, isset) {
    if (!isset) {
      return self.setnx(key, value, cb);
    }
    return cb(null, false);
  });
};

EventedStoreMemory.prototype.get = function(key, cb) {
  var self = this;
  this.isset(key, function(err, isset) {
    if (!isset) {
      return cb && cb( new Error('key not set'));
    }
    if (typeof self.ex[ key ] !== 'undefined' && (self.ex[key] < (+new Date()) ) ) {
      self.debug && console.log('expired key get', key, self.ex[key] );
      self.unset(key, function(err, r) {
        return cb && cb( new Error('key expired') );
      });
      return ;
    }

    cb && cb(null, self.cache[key]);
  });
};
EventedStoreMemory.prototype.unset = function(key, cb) {
  var self = this;
  self.get(key, function(err, value) {
    if (err || !value) {
      return cb && cb( new Error('key not set'));
    }
    self.emit('unset', key, value);
    delete( self.cache[key] );
    return cb && cb();
  });
};
// modelled on redis' HSET - set a field in a hash
EventedStoreMemory.prototype.hset = function(key, field, value, cb) {
  var self = this;
  this.get(key, function(err, data) {
    if (err || !data ) {
      data = {};
    }
    data[ field ] = value;
    self.set( key, data, cb );
  });
}
// modelled on redis' HGET - set a field in a hash
EventedStoreMemory.prototype.hget = function(key, field, cb) {
  var self = this;
  this.get(key, function(err, data) {
    if (err || !data) {
      self.debug && console.log('hget failed no', key, err, data);
      return cb(err, null);
    }
    self.debug && console.log('hget', key, field);
    if ( typeof data[ field ] != 'undefined') {
      return cb(null, data[ field ]);
    } else {
      return cb(null, null);
    }
  });
}

EventedStoreMemory.prototype.keys = function(cb) {
  cb(this.cache);
}

exports.EventedStoreMemory = EventedStoreMemory;