/*
 * Very basic (in-memory) asynchronous key-value store extending an EventEmitter
 * Should be easily extended for persistence backends
 *
 * @todo: wrapped this around the keys module or node-dirty or whateva ...
 */
var events = require('events');

exports.EventedStore = {
  createStore: function(type, options) {
    if (typeof type == 'object') {
      options = type;
      type = type.type;
    }

    switch (type) {
      case 'redis':
        var EventedStoreRedis = require('./eventedstore_redis.js').EventedStoreRedis;
        return new EventedStoreRedis(options);
        break;
      case 'memory':
      default:
        var EventedStoreMemory = require('./eventedstore_memory.js').EventedStoreMemory;
        return new EventedStoreMemory(options);
    }
  }
};
