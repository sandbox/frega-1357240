<?php
/**
 * @file
 * API documentation for dnode_faye.
 */

/**
 * Alter the channels that the current users /
 *
 * You could use this to whitelist or blacklist
 *
 *
 * @see dnode_faye.module
 */
function hook_dnode_faye_subscriptions_alter(&$subscriptions) {
  // $subscriptions array a keyed array, currently the value is always TRUE,
  // let's see if we can come up with a use-
  // ful usage (e.g. like #attaching js?)
  // array('/channel' => TRUE, '/channel2');
}
