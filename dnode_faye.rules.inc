<?php
/**
 * @file
 * Rules file for the dnode_faye module.
 */

/**
 * Implements hook_rules_event_info().
 *
 * This function declares all dnode_faye_rules actions
 */
function dnode_faye_rules_action_info() {
  // we're only implementing actions for the node entity-type
  return array(
    'dnode_faye_publish_json' => array(
      'label' => t('dnode_faye: Send a (JSON) message to a channel'),
      'parameter' => array(
        'channel' => array(
          'type' => 'text',
          'label' => t('Channel to send to'),
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('JSON message'),
          'optional' => TRUE,
        ),
        'callback' => array(
          'type' => 'text',
          'label' => t('PHP function name to handle the callback (optional)'),
          'optional' => TRUE,
        ),
      ),
      'group' => t('dnode_faye'),
      'base' => 'dnode_faye_publish_json',
      'callbacks' => array(),
    ),
  );
}

/**
 * Rules action callback to publish json.
 */
function dnode_faye_publish_json($channel, $message = array(), $cb = NULL, $element = NULL) {
  if (!is_array($message)) {
    $message = json_decode($message);
  }
  dnode_faye_publish($channel, $message, $cb);
}
