Drupal.behaviors.faye = {
  attach: function (context, settings) {
    var clientAuth = {
      incoming: function(message, callback) {
        // we do this, make sure we always know which channel the message got to
        if (message.data && !message.data.channel) {
          message.data.channel = message.channel;
        }
        // let's see if we have a successful subscription and backlog data
        if (message.channel == '/meta/subscribe' && message.successful && message.ext && message.ext.backlog) {
          // @todo: wrap this? to make sure this bulk update gets priority treatment?
          for (var i=0, len = message.ext.backlog.length; i<len; i++) {
            var m = message.ext.backlog[i];
            // let's set the channel appropriated
            m.channel = message.subscription;
            // use the .ext property
            m.ext = m.ext || {};
            // signal to the handler that we handling with a backlog
            m.ext.backlog = {offset: i, length: len};
            Drupal.Faye.defaultHandler(m);
          }
        }
        callback(message);
      },
      outgoing: function(message, callback) {
        // Again, leave non-subscribe messages alone
        if (message.channel !== '/meta/subscribe')
          return callback(message);
        // Add ext field if it's not present
        if (typeof settings.Faye.subscriptions[message.subscription] !== 'undefined') {
          if (!message.ext) message.ext = {};
          // Set the auth token
          message.ext.authToken = settings.Faye.subscriptions[message.subscription];
        }
        // Carry on and send the message to the server
        callback(message);
      }
    };
    if (typeof Faye == 'undefined') {
      if (typeof console != 'undefined') {
        console.error(Drupal.t('Faye client could not be loaded. Please check the Faye server is running.'));
      }
      return false;
    }
    Drupal.Faye.client = new Faye.Client(settings.dnode_faye_url);
    Drupal.Faye.client.addExtension(clientAuth);
    if (settings.Faye && settings.Faye.subscriptions) {
      for (channel in settings.Faye.subscriptions) {
        Drupal.Faye.client.subscribe(channel, Drupal.Faye.defaultHandler);
      }
    }
  }
};

Drupal.Faye = Drupal.Faye || {observers:{}};
Drupal.Faye.defaultHandler = function(data) {
  for (name in Drupal.Faye.observers) {
    channel = data.channel || '';
    Drupal.Faye.observers[name].callback(channel, data);
  }
};

Drupal.Faye.addObserver = function(name, cb, channel) {
  Drupal.Faye.observers[name] = {callback: cb, channel: channel || []};
}

Drupal.Faye.removeObserver = function(name) {
  delete Drupal.Faye.observers[name];
}
