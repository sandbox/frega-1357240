This is a small module that illustrates how to write a small plugin calling some
dnode server from within nodejs.

You'll want to disable this module quickly. It merely pushes out a random
bieber-tweet every 5 seconds or so.
