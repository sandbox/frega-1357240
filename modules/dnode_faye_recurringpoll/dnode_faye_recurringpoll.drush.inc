<?php
/**
 * @file
 * Drush command file for the dnode_faye_recurringpoll module.
 */

/**
 * Implements hook_drush_command().
 */
function dnode_faye_recurringpoll_drush_command() {
  $items['dnode-faye-recurringpoll-start'] = array(
    'callback' => 'drush_dnode_faye_recurringpoll_start',
    'description' => 'Start a recurring poll in the backend',
  );
  $items['dnode-faye-recurringpoll-stop'] = array(
    'callback' => 'drush_dnode_faye_recurringpoll_stop',
    'description' => 'Start a recurring poll in the backend',
  );
  $items['dnode-faye-recurringpoll-test'] = array(
    'callback' => 'drush_dnode_faye_recurringpoll_test',
    'description' => 'Demonstrates the example block :)',
  );
  return $items;
}

/**
 * Start a recurring poll
 */
function drush_dnode_faye_recurringpoll_start($channel, $url, $frequency) {
  drush_print("Start polling and publishing $channel:$url every $frequency ms");
  dnode_rpc('dnode_faye_recurringpoll', 'startRecurringPoll',
    array($channel, $url, $frequency)
  );
}

/**
 * Stop a recurring poll
 */
function drush_dnode_faye_recurringpoll_stop($channel) {
  drush_print("Stop polling and publishing $channel");
  dnode_rpc('dnode_faye_recurringpoll', 'stopRecurringPoll',
    array($channel)
  );
}

/**
 * Test a recurring poll
 */
function drush_dnode_faye_recurringpoll_test($channel = FALSE) {
  drush_print(t('Make sure you have the example block visible somewhere ... '));
  $channel = $channel ? $channel : _dnode_faye_get_channel_by_csi_id(variable_get('dnode_faye_csi_block_example', 'example'));

  dnode_faye_setChannelBacklog($channel, 1);
  drush_print(t('Enabled channel backlog for @channel', array('@channel' => $channel)));
  while (TRUE) {
    $delay = rand(1, 10);
    $random_message = t("Hello, it's @time here - i will get back to you in @secs second(s)",
      array('@time' => date('Ymd His'), '@secs' => $delay)
    );
    dnode_faye_publish($channel, array('data' => $random_message, 'callback' => 'dnode_faye_csi'));
    drush_print(t('Published to @channel: @random_message', array('@channel' => $channel, '@random_message' => $random_message)));
    sleep($delay);
  }
}
