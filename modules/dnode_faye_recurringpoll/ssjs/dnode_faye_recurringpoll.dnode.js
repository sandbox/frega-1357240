//
var dnode = require('dnode');
var sys = require('util'),
    rest = require('restler'),
    crypto = require('crypto'),
  http = require('http'),
  // @todo: turn this into an npm
    EventedStore = require('../../../ssjs/eventedstore').EventedStore;

var debug = true;
var intervalCache = EventedStore.createStore('memory', {});
var responseCache = EventedStore.createStore('memory', {});

// start listening to the caches
responseCache.on('set', function(k,v) {
  debug && console.log('client.publish(' + k + ')', v.data);
  publish(k,v);
});

intervalCache.on('unset', function(k,v) {
  debug && console.log('intervalCache.unset ' + k + ')', v );
  clearInterval( v );
});

function publish(channel, data) {
  // simply use dnode for this
  dnode.connect(5051, function (remote, conn) {
    debug && console.log('dnode_faye_recurringpoll::publish ', channel, data);
    data.callback = 'dnode_faye_csi';
    remote.publish( channel, data, function() {
      conn.end();
    });
  });
}

exports.startRequest = function(id, url, options, cb) {
  intervalCache.isset(id, function(err, isset) {
    if (isset) {
      // this should be run asynchronously but doesnt do much harm, ey.
      exports.stopRequest(id, function() {});
      debug && console.log('overwriting existing request', id);
    }
    options = options || {};
    options.method = options.method || 'get';
    options.frequency || 1000;
    var info = {
        id: id,
        url: url,
        options: options
    };
    var interval = setInterval( function(info) {
      console.log('start', info);
      rest.request( info.url, info.options ).on('complete', function(data, response) {
        console.log('complete', response.statusCode, data);
        // if we have a css selector?
        responseCache.get(info.id, function(err, cachedData) {
          if (err || cachedData.data != data) {
            responseCache.set(id, {
              info: info,
              data: data,
              mtime: (+new Date())
            }, function() {
              debug && console.log('after setting responseCacheData');
            });
          } else {
            debug && console.log('cache is uptodate');
          }
        });
      }).on('error', function() {
          console.log('ERROR' , arguments);
      }).on('close', function() {
          console.log('CLOSE' , arguments);
        })
    }, options.frequency, info);
    intervalCache.set( id, interval, function(err, data) {
      return cb && cb(err, data);
    });

  });
};

exports.stopRequest = function(id, cb) {
  intervalCache.unset( id, function(err, data) {
    if (err) {
      return cb && cb( new Error('could not unset request interval') );
    }
    responseCache.unset( id, function(err, data) {
      if (err) {
        return cb && cb( new Error('could not unset request interval') );
      }
      return cb && cb(null);
    });
  });
}

// we want
exports.startRecurringPoll = function( channel, url, frequency, cb ) {
  options = {
    frequency: frequency,
    method: 'get'
  };
  exports.startRequest(channel, url, options, cb);
}

exports.stopRecurringPoll = function( channel, cb ) {
  exports.stopRequest(channel, cb);
}

// startRecurringMemcachePoll = function(id, key, options) {




// expose configuration
exports.config = {
  serverId: 'dnode_faye_recurringpoll',
  port: 5053
};
