<?php
/**
 * @file
 * Theme file for the dnode_faye_csi module.
 */


/**
 * Generic <div> container function.
 */
function theme_dnode_csi_container($variables) {
  $element = $variables['element'];
  return '<div' . drupal_attributes($element['#attributes']) . '>' . $element['#children'] . '</div>';
}
