<?php
/**
 * @file
 * Drush command file for the dnode_faye_csi module.
 */

/**
 * Implements hook_drush_command().
 */
function dnode_faye_csi_drush_command() {
  $items['dnode-faye-csi-publish'] = array(
    'callback' => 'drush_dnode_faye_csi_publish',
    'description' => 'Publish HTML or something in to the a dnode_faye_csi-block (defaults to /dnode_faye_csi/public)',
  );
  return $items;
}

/**
 * Publish a message/HTML
 */
function drush_dnode_faye_csi_publish($message, $channel = '/dnode_faye_csi/public', $enable_backlog = 1) {
  if ($enable_backlog) {
    dnode_rpc('dnode_faye', 'setChannelBacklog', array($channel, $enable_backlog), FALSE);
    if ($enable_backlog) {
      drush_print(dt('Enabled channel backlog for @channel, backlogSize @backlog_size',
        array('@channel' => $channel, '@backlog_size' => $enable_backlog))
      );
    }
    else {
      drush_print(dt('Disabled channel backlog for ' . $channel));
    }
  }
  drush_print(dt("Sending to @channel message: @message",
    array('@channel' => $channel, '@message' => $message))
  );
  dnode_faye_publish($channel, array('data' => $message));
}
