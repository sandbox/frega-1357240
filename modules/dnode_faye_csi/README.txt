A small library using dnode_faye to allows you to "pipe in" content into a fully
rendered drupal page via a pubsub pattern rather than AJAX.
