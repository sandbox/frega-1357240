(function ($) {
  var $wrapperCache = {};

  Drupal.behaviors.dnode_faye_csi = {
    attach: function() {
      $('.dnode-faye-csi-wrapper').once(function() {
        var csi_channel = $(this).data('dnode-faye-csi-channel');
        $wrapperCache[csi_channel] = $(this);
      });
    }
  }

  Drupal.Faye.addObserver('dnode_faye_csi', function(channel, message) {
    if (channel == '/dnode_faye_csi/public' || message.callback == 'dnode_faye_csi') {
      if (channel && typeof $wrapperCache[channel] !== 'undefined') {
        $wrapperCache[channel].html(message.data);
      }
    }
  });
})(jQuery);
