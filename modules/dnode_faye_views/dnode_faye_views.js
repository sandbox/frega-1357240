(function ($) {
  var $wrapperCache = {};

  Drupal.behaviors.dnode_faye_views = {
    attach: function() {
      $('.dnode-faye-views-wrapper').once(function() {
        var viewId = $(this).data('dnode-faye-views-id');
        $wrapperCache[viewId] = $(this);
      });
    }
  }

  Drupal.Faye.addObserver('dnode_faye_views', function(channel, message) {
    if (message && message.callback == 'dnode_faye_views' && typeof message.view_id !== 'undefined') {
      var view_id = message.view_id;
      if (view_id && typeof $wrapperCache[view_id] !== 'undefined') {
        $wrapperCache[view_id].html(message.data);
      }
    }
  });
})(jQuery);
