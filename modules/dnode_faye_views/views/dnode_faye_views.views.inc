<?php
/**
 * @file
 * File for dnode_faye_views views plugin.
 */

/**
 * Implements hook_views_plugins().
 */
function dnode_faye_views_views_plugins() {
  $x = array(
    'cache' => array(
      'dnode_faye_views_plugin_cache' => array(
        'path' => drupal_get_path('module', 'dnode_faye_views') . '/views',
        'title' => t('Time-based (broadcast via faye-pubsub)'),
        'handler' => 'dnode_faye_views_plugin_cache',
        'uses options' => TRUE,
      ),
    ),
  );
  return $x;
}
