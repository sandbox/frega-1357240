<?php
/**
 * @file
 * Cache plugin for dnode_faye_views views plugin.
 */

/**
 * Simple caching of query results for Views displays.
 */
class dnode_faye_views_plugin_cache extends views_plugin_cache_time {
  /**
   * Information about options for all kinds of purposes will be held here.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['results_channel'] = array('default' => '');
    $options['results_channel_callback'] = array('default' => '');
    $options['output_channel'] = array('default' => '/dnode_faye_views');
    $options['output_channel_callback'] = array('default' => 'dnode_faye_views');
    return $options;
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    $options = array(1, 5, 10, 20, 30, 60, 300, 1800, 3600, 21600, 518400);
    $options = drupal_map_assoc($options, 'format_interval');
    $options = array(-1 => t('Never publish results (defeats the purpose entirely, btw :)')) + $options;

    $form['results_lifespan'] = array(
      '#type' => 'select',
      '#title' => t('Query results'),
      '#description' => t('The length of time raw query results should be cached.'),
      '#options' => $options,
      '#default_value' => $this->options['results_lifespan'],
    );
    $form['results_channel'] = array(
      '#type' => 'textfield',
      '#title' => t('Faye Channel for results'),
      '#description' => t('Faye Channel to which the results will be published.'),
      '#default_value' => $this->options['results_channel'],
    );
    $form['results_channel_callback'] = array(
      '#type' => 'textfield',
      '#title' => t('Faye Channel callback/tag for results'),
      '#default_value' => $this->options['results_channel_callback'],
    );

    $form['output_lifespan'] = array(
      '#type' => 'select',
      '#title' => t('Rendered output'),
      '#description' => t('The length of time rendered HTML output should be cached.'),
      '#options' => $options,
      '#default_value' => $this->options['output_lifespan'],
    );
    $form['output_channel'] = array(
      '#type' => 'textfield',
      '#title' => t('Faye Channel for rendered output'),
      '#description' => t("Channel to which the output will be rendered (leave to default, unless you're implementing your own client-side logic."),
      '#default_value' => $this->options['output_channel'],
    );
    $form['output_channel_callback'] = array(
      '#type' => 'textfield',
      '#title' => t("Faye Channel callback/tag for output (leave to default, unless you're implementing your own client-side logic)."),
      '#default_value' => $this->options['output_channel_callback'],
    );
  }

  /**
   * Return a string to display as the clickable title for the
   * access control.
   */
  function summary_title() {
    return format_interval($this->options['results_lifespan'], 1) . '->' . ($this->options['results_channel'] ? $this->options['results_channel'] : t('Results not published via dnode_faye')) .
              ' | ' . format_interval($this->options['output_lifespan'], 1) . ' -> ' .
              ($this->options['output_channel'] ? $this->options['output_channel'] : t('Output not published via dnode_faye')) ;
  }

  /**
   * Save data to the cache.
   *
   * A plugin should override this to provide specialized caching behavior.
   */
  function cache_set($type) {
    switch ($type) {
      /* case 'query':
        // Not supported currently, but this is certainly where we'd put it.
        break; */
      case 'results':
        parent::cache_set($type);
        $data = array(
          'result' => $this->view->result,
          'total_rows' => isset($this->view->total_rows) ? $this->view->total_rows : 0,
          'current_page' => $this->view->get_current_page(),
        );
        if ($this->options['results_channel']) {
          $options = $this->options;
          $data['callback'] = isset($options['results_channel_callback']) ? $options['results_channel_callback'] : NULL;
          dnode_faye_publish($options['results_channel'], $data);
        }
        break;

      case 'output':
        $view_id = $this->view->name . '-' . $this->display->id;
        // Hmm adding stuff in the caching layer, doesn't feel right.
        $this->view->display_handler->output = '<div class="dnode-faye-views-wrapper" data-dnode-faye-views-id="' . $view_id . '" data-dnode-faye-views-channel="' . trim($this->options['output_channel']) . '">' .
          $this->view->display_handler->output . '</div>';
        parent::cache_set($type);
        if ($this->options['output_channel']) {
          $options = $this->options;
          $data = array();
          $data['callback'] = isset($options['output_channel_callback']) ? $options['output_channel_callback'] : NULL;
          $data['data'] = $this->view->display_handler->output;
          $data['view_id'] = $view_id;
          dnode_faye_publish($options['output_channel'], $data);
        }
        break;

    }
  }

  /**
   * Retrieve data from the cache.
   *
   * A plugin should override this to provide specialized caching behavior.
   */
  function cache_get($type) {
    if ($type == 'output') {
      drupal_add_js(drupal_get_path('module', 'dnode_faye_views') . '/dnode_faye_views.js');
      dnode_faye_add_subscription($this->options['output_channel']);
      return parent::cache_get($type);
    }
    else {
      return parent::cache_get($type);
    }
  }
}
