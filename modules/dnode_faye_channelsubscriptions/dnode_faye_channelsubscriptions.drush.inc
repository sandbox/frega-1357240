<?php
/**
 * @file
 * Drush commands file for the dnode_faye_channelsubscriptions module.
 */

/**
 * Implements hook_drush_command().
 */
function dnode_faye_channelsubscriptions_drush_command() {
  $items['dnode-faye-cs-add'] = array(
    'callback' => 'dnode_faye_channelsubscriptions_add',
    'description' => 'Add a persistent subscription',
  );
  $items['dnode-faye-cs-remove'] = array(
    'callback' => 'dnode_faye_channelsubscriptions_remove',
    'description' => 'Remove a persistent subscription',
  );
  return $items;
}
