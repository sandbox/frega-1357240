<?php
/**
 * @file
 * Rules file for the dnode_faye_channelsubscriptions module.
 */

/**
 * Implements hook_rules_event_info().
 *
 * This function declares all dnode_faye_rules actions
 */
function dnode_faye_channelsubscriptions_rules_action_info() {
  return array(
    'dnode_faye_channelsubscriptions_add' => array(
      'label' => t('dnode_faye_channelsubscription: add a (persistent) subscription'),
      'parameter' => array(
        'subscriber_type' => array(
          'type' => 'text',
          'label' => t('Subscribe type (user|role)'),
          'optional' => FALSE,
        ),
        'subscriber_id' => array(
          'type' => 'text',
          'label' => t('Subscriber id (1)'),
          'optional' => FALSE,
        ),
        'channel' => array(
          'type' => 'text',
          'label' => t('Channel'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('dnode_faye_channelsubscriptions'),
      'base' => 'dnode_faye_channelsubscriptions_add',
      'callbacks' => array(),
    ),
    'dnode_faye_channelsubscriptions_remove' => array(
      'label' => t('dnode_faye_channelsubscription: remove a (persistent) subscription'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User to notify'),
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject to send'),
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Text to send'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('dnode_faye_channelsubscriptions'),
      'base' => 'dnode_faye_channelsubscriptions_remove',
      'callbacks' => array(),
    ),
  );
}
