var dnode = require('dnode');
// bad programmer, configuration here, no cookie.
var config = {
  'channelBacklog': 10,
  'uppercaseMessages': false,
  'publicSubscription': true,
  'retrieveUsername': true,
  'filter': 'bieber'
};
var debug = false;

var chatterboxExtension = {
  // every incoming message passes through this *ONCE*
  'incoming': function(message, callback) {
    if ((message.channel == '/dnode_faye_chatterbox') && (!message.error)) {
      debug && console.log('MESSAGE - incoming chatterbox', message);
      // filter, if filter is set => demonstrate aborting messages
      if (
        config.filter &&
        message.data.body.toLowerCase().indexOf(config.filter.toLowerCase()) != -1
      ) {
        debug && console.log('MESSAGE - FILTERED!', message);
        message.error = config.filter + ' must not be in message!';
        return callback(message);
      }
      // everybody SHOUTING UPPERCASE.
      if (config.uppercaseMessages && message.data.body) {
        message.data.body = message.data.body.toUpperCase();
      }
      // ensure user IS correct
      if (config.retrieveUsername) {
        dnode.connect(5051, function (remote) {
          remote.getUserDataByClientId(message.clientId, function(err, data) {
            if (err || !data) {
              debug && console.log("Invalid clientId ", err);
              message.error = 'Invalid clientId!';
            } else {
              debug && console.log('chatterboxExtension retrieved user data by clientId', message, data);
              if (message.data.name != data.name) {
                message.data.name = 'YOU ARE LYING - YOU ARE ' + data.name + ' + NOT ' + message.data.name;
              }
            }
            callback(message);
          });
        });
      } else {
        callback(message);
      }
    } else {
      debug && console.log('MESSAGE - incoming chatterbox', message);
      callback(message);

    }
  },
  // every outgoing message passes through this *ONCE*
  'outgoing': function(message, callback) {
    if (message.channel == '/dnode_faye_chatterbox' && !message.error && message.data) {
      // add something, like when did this message "leave the server" (for lag/jitter calculation).
      message.data.ts = +(new Date());
    }
    debug && console.log('MESSAGE - outgoing chatterbox', message);
    callback(message);
  }
}

// add the chatterboxExtension
process.nextTick(function() {
  dnode.connect(5051, function (remote) {
    remote.addExtension(chatterboxExtension, function() {
      debug && console.log('dnode_faye_chatterbox - added extension chatterboxExtension');
    });
    remote.setChannelBacklog('/dnode_faye_chatterbox', 10, function() {
      debug && console.log('dnode_faye_chatterbox - enabled channel backlog of 10 for channel /dnode_faye_chatterbox');
    });

    if (config.publicSubscription) {
      // the default publicSubscription means, authenticated users can_subscribe but not publish.
      remote.setChannelDefaultPermissions('/dnode_faye_chatterbox', {'can_publish': false, 'can_subscribe': true}, function() {
        debug && console.log('dnode_faye_chatterbox - setChannelDefaultPermissions to public subscription');
      });
    } else {
      remote.setChannelDefaultPermissions('/dnode_faye_chatterbox', {'can_publish': false, 'can_subscribe': false}, function() {
        debug && console.log('dnode_faye_chatterbox - setChannelDefaultPermissions to NOT ALLOW PUBLIC SUBS');
      });
    }
  });
});
