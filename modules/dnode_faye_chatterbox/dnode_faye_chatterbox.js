(function($, Drupal){
  Drupal.behaviors.dnode_faye_chatterbox = {
    attach: function (context, settings) {
      $('#dnode-faye-chatterbox-wrapper').once(function() {
        $('form', $(this)).submit(function(e) {
          var v = $('#dnode-faye-chatterbox-text').val();
          if (v) {
            Drupal.Faye.client.publish('/dnode_faye_chatterbox', {
              'body': v,
              'name': settings.dnode_faye_chatterbox.name,
              'callback': 'dnode_faye_chatterbox'
            });
          }
          $('#dnode-faye-chatterbox-text').val('');
          return false;
        });
        Drupal.Faye.addObserver('dnode_faye_chatterbox', function(channel, message) {
          if (message.callback == 'dnode_faye_chatterbox') {
            var n = $("#dnode-faye-chatterbox li").length;
            if (n >= 10) {
              $('#dnode-faye-chatterbox li:last').remove();
            }
            var c;
            if (message.name) {
              c = '<strong> ' + Drupal.checkPlain(message.name) + '</strong>: ';
            } else {
              c = '';
            }
            $('<li>' + c + Drupal.checkPlain(message.body) + '</li>')
                    .prependTo($('#dnode-faye-chatterbox'));
          }
        });
      });
    }
  };
})(jQuery, Drupal);
