<?php
/**
 * @file
 * Drush command file for the dnode_faye_chatterbox module.
 */

/**
 * Implements hook_drush_command().
 */
function dnode_faye_chatterbox_drush_command() {
  $items['dnode-faye-chatterbox-say'] = array(
    'callback' => 'drush_dnode_chatterbox_say',
    'description' => 'Say something to the chatterbox (optionally give a name)',
  );
  return $items;
}

/**
 * Chatterbox command callback
 */
function drush_dnode_chatterbox_say($message, $name = 'SERVER') {
  $channel = '/dnode_faye_chatterbox';
  drush_print("Sending to /dnode_faye_chatterbox - IMPORTANT NOTE: this will fail, if 'retrieveUsername' has been enabled in the module!");
  dnode_faye_publish(
    $channel,
    array(
      'name' => $name,
      'body' => $message,
      'callback' => 'dnode_faye_chatterbox',
    )
  );
}
