(function ($) {
  Drupal.Faye.addObserver('dnode_faye_notify', function(channel, message) {
    if (channel == '/dnode_faye_notify' || channel.indexOf('/user/') !== -1 || message.callback == 'dnode_faye_notify') {
      $.jGrowl(message.body, {header: message.subject, life:(3000)});
    }
  });
})(jQuery);
