<?php
/**
 * @file
 * Drush command file for the dnode_faye_notify module.
 */

/**
 * Implements hook_drush_command().
 */
function dnode_faye_notify_drush_command() {
  $items['dnode-faye-notify'] = array(
    'callback' => 'dnode_faye_notify',
    'description' => 'Publish a message to everyone!',
    'options' => array(
      'subject' => 'Subject',
      'body' => 'Body',
    ),
  );
  $items['dnode-faye-notify-user'] = array(
    'callback' => 'drush_dnode_faye_notify_user',
    'description' => 'Publish a message to one uid!',
    'options' => array(
      'uid' => 'User ID',
      'subject' => 'Subject',
      'body' => 'Body',
    ),
  );
  return $items;
}

/**
 * Broadcast
 */
function dnode_faye_notify($subject, $body) {
  dnode_faye_notify_broadcast($subject, $body);
}

/**
 * Notify on user
 */
function drush_dnode_faye_notify_user($uid, $subject, $body) {
  dnode_faye_notify_user($uid, $subject, $body);
}
