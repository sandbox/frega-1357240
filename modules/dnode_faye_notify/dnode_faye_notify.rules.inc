<?php
/**
 * @file
 * Rules file for the dnode_faye_notify module.
 */

/**
 * Implements hook_rules_event_info().
 *
 * This function declares all dnode_faye_rules actions
 */
function dnode_faye_notify_rules_action_info() {
  // we're only implementing actions for the node entity-type
  return array(
    'dnode_faye_notify_broadcast' => array(
      'label' => t('dnode_faye_notify: Broadcast a message to everyone'),
      'parameter' => array(
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject to broadcast'),
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Text to broadcast'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('dnode_faye_notify'),
      'base' => 'dnode_faye_notify_broadcast_rules',
      'callbacks' => array(),
    ),
    'dnode_faye_notify_user' => array(
      'label' => t('dnode_faye_notify: Send a message to a user'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User to notify'),
        ),
        'subject' => array(
          'type' => 'text',
          'label' => t('Subject to send'),
          'optional' => FALSE,
        ),
        'message' => array(
          'type' => 'text',
          'label' => t('Text to send'),
          'optional' => FALSE,
        ),
      ),
      'group' => t('dnode_faye_notify'),
      'base' => 'dnode_faye_notify_user_rules',
      'callbacks' => array(),
    ),
  );
}

/**
 * Notify user
 */
function dnode_faye_notify_user_rules($user, $subject, $message, $element = NULL) {
  return dnode_faye_notify_user($user->uid, $subject, $message);
}

/**
 * Broadcast
 */
function dnode_faye_notify_broadcast_rules($subject, $message, $element = NULL) {
  return dnode_faye_notify_broadcast($subject, $message);
}
